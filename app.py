from flask import Flask
from flask import request
from nfl_event import NflEvent
import moment

nflEvent = NflEvent()
app = Flask(__name__)


def dateValidation(startDate, endDate):
    try:
        if moment.utc(endDate).diff(moment.utc(startDate), 'days').days >= 0:
            return True
    except Exception as ex:
        print(ex)
        return False


@app.route("/", methods=['GET'])
def get():
    try:
        global nflEvent
        startDate = ''
        endDate = ''
        if 'startDate' in request.args:
            startDate = request.args['startDate']
        if 'endDate' in request.args:
            endDate = request.args['endDate']

        if dateValidation(startDate, endDate):
            result = nflEvent.getCombinedResult(startDate, endDate)
        else:
            return {"error": "End date should be greater than or equal to start date"}
        return {"result": result}
    except Exception as ex:
        print(ex)
        return {"error": "Failed"}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80)
